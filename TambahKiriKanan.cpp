#include <iostream>
using namespace std;

struct Tree{
    char huruf;
    Tree *left;
    Tree *right;
};
Tree *nodebaru, *leaf;

void preOrder(Tree *current){
    if(current != NULL){
        cout << current->huruf << " ";
        preOrder(current->left);
        preOrder(current->right);
    }
}

void inOrder(Tree *current){
    if(current != NULL){
        inOrder(current->left);
        cout << current->huruf << " ";
        inOrder(current->right);
    }
}

void postOrder(Tree *current){
    if(current != NULL){
        postOrder(current->left);
        postOrder(current->right);
        cout << current->huruf << " ";
    }
}

Tree *createTree(char huruf){
        nodebaru = new Tree();
        nodebaru->huruf = huruf;
        nodebaru->left = NULL;
        nodebaru->right = NULL;

        return nodebaru;
}

void tambahChild(Tree *&leaf, char huruf, string pos){
    if(leaf == NULL){
        leaf = createTree(huruf);
    }else if(pos == "kiri"){
        tambahChild(leaf->left, huruf, pos);
    }else{
        tambahChild(leaf->right, huruf, pos);
    }
}

void menu(int pilih){
    char data;
    switch(pilih)
    {
        case 1:
            cout<<"Masukkan satu huruf: ";
            cin>>data;
            tambahChild(leaf, data, "kiri");
            break;
        case 2:
            cout<<"Masukkan satu huruf: ";
            cin>>data;
            tambahChild(leaf, data, "kanan");
            break;
        case 3: preOrder(leaf);break;
        case 4: inOrder(leaf);break;
        case 5: postOrder(leaf);break;
        case 0: exit(0);break;
    }
}

int main(){
    cout << "Nama \t:Ghanang Adhin Nugroho\n";
    cout << "NIM \t:A11.2021.13914\n";
    cout << "Kelp. \t:Struktur Data - A11.4302\n";
    cout << "==================================\n";

    leaf = NULL;
    int pilih;
    do{
        cout << "\n";
        cout << "1. Tambah Kiri\n";
        cout << "2. Tambah Kanan\n";
        cout << "3. Pre Order\n";
        cout << "4. In Order\n";
        cout << "5. Post Order\n";
        cout << "0. Exit\n";
        cout << "Masukkan Pilihan Anda: ";
        cin >> pilih;
        menu(pilih);
    } while(pilih != 0);

    return 0;
}
