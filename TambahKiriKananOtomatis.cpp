#include <iostream>
using namespace std;

struct Tree{
    char data;
    Tree *left;
    Tree *right;
};
Tree *nodebaru, *leaf;

void preOrder(Tree *current){
    if(current != NULL){
        cout << current->data << " ";
        preOrder(current->left);
        preOrder(current->right);
    }
}

void inOrder(Tree *current){
    if(current != NULL){
        inOrder(current->left);
        cout << current->data << " ";
        inOrder(current->right);
    }
}

void postOrder(Tree *current){
    if(current != NULL){
        postOrder(current->left);
        postOrder(current->right);
        cout << current->data << " ";
    }
}

Tree *createTree(int data){
        nodebaru = new Tree();
        nodebaru->data = data;
        nodebaru->left = NULL;
        nodebaru->right = NULL;

        return nodebaru;
}

void createLeaf(Tree *&leaf, int data){
    if(leaf == NULL){
        leaf = createTree(data);
    }else if(leaf->data > data){
        createLeaf(leaf->left, data);
    }else{
        createLeaf(leaf->right, data);
    }
}

void menu(int pilih)
{
    char data;
    switch(pilih)
    {
        case 1:
            cout<<"Masukkan satu huruf: ";
            cin>>data;
            createLeaf(leaf, data);
            break;
        case 2: preOrder(leaf);break;
        case 3: inOrder(leaf);break;
        case 4: postOrder(leaf);break;
        case 0: exit(0);break;
    }
}

int main(){
    cout << "Nama \t:Ghanang Adhin Nugroho\n";
    cout << "NIM \t:A11.2021.13914\n";
    cout << "Kelp. \t:Struktur Data - A11.4302\n";
    cout << "==================================\n";

    leaf = NULL;
    int pilih;
    do{
        cout << "\n";
        cout << "1. Tambah Data\n";
        cout << "2. Pre Order\n";
        cout << "3. In Order\n";
        cout << "4. Post Order\n";
        cout << "0. Exit\n";
        cout << "Masukkan Pilihan Anda: ";
        cin >> pilih;
        menu(pilih);
    } while(pilih != 0);

    return 0;
}
